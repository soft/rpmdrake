��    B     ,    <$      P0     Q0  "   i0  2   �0  <   �0  T   �0  N   Q1  -   �1  J   �1  E   2  M   _2  6   �2  8   �2  g   3  >   �3  -   �3  -   �3  R    4  I   s4  :   �4     �4     �4  #   5     ,5     F5     d5     j5  )   |5     �5  !   �5     �5     �5     6     6     '6     E6     L6     Z6     b6  
   }6     �6     �6     �6     �6     �6     �6  	   �6  
   �6     �6     7     7  $   )7  &   N7     u7     ~7     �7  
   �7  
   �7  
   �7  
   �7  
   �7  
   �7  
   �7  
   �7  
   �7     �7  A   8  D   U8     �8     �8  
   �8  	   �8     �8     �8     �8     �8     9     9     9     &9  	   A9     K9  @   O9     �9     �9  "   �9      �9     :  $   :  3   A:     u:     �:     �:     �:     �:     �:     �:     �:  	   �:  ,   �:  6   ;  P   ;;  	   �;  	   �;     �;  	   �;     �;     �;  P   �;  Q   <     a<     i<     q<     x<     ~<     �<     �<  	   �<     �<     �<     �<     �<     �<     �<     �<     �<     �<  
    =     =  
   =     $=     0=     9=  #   >=  *   b=  	   �=     �=  (   �=  .   �=     �=  -   >     5>     =>     E>     T>     `>     o>     �>     �>  9   �>     �>     �>     �>     ?     ?     "?     B?  
   [?  %   f?     �?     �?     �?     �?     �?  	   �?  +   �?     @     @     @     '@     3@  
   K@     V@     r@     �@  (   �@     �@     �@     �@     A     	A     A     -A     5A     JA     gA  	   oA  	   yA     �A     �A     �A     �A     �A  *   �A      �A  '   B     CB  
   GB  
   RB     ]B     xB     }B  
   �B     �B     �B     �B     �B     �B     �B     �B     �B  5   �B  d   C     wC     ~C     �C     �C     �C     �C     �C     �C     �C     �C  "   �C  1   D  '   ED     mD     �D     �D     �D     �D     �D     �D     �D     �D     �D     �D     �D     �D     �D  ,   E     CE  "   JE  0   mE     �E     �E  !   �E  �   �E  }   �F  �   G  �   �G     "H     &H     4H  Z   :H     �H  �  �H  I   )J     sJ     {J     �J     �J  
   �J     �J  !   �J     �J     K     K  	   .K  "   8K     [K     mK     �K     �K     �K  #   �K      �K  %  �K     M     !M     'M     ,M     0M  
   @M     KM     RM     fM  #   lM  	   �M  
   �M     �M     �M     �M     �M     �M     �M  .   �M     $N     7N     CN     ON     \N     cN     pN     }N     �N     �N     �N  
   �N  	   �N     �N  
   �N     �N      O     O     	O     O     O     6O  
   NO     YO     _O     qO     vO  M   yO     �O  	   �O     �O     �O  Q   P  	   bP  ?   lP  G   �P     �P     Q     Q     Q     'Q     0Q     7Q     JQ  	   MQ  (   WQ     �Q     �Q     �Q     �Q     �Q     �Q  �   �Q     �R  	   �R  	   �R     �R     �R     �R     �R  !   �R  2   S  #   HS  G   lS     �S     �S  @   �S  *   T  *   IT  *   tT  %   �T      �T     �T  !   U     (U     BU     aU     hU  -   qU  !   �U     �U     �U     �U     �U  	   V     V      V     >V     GV  
   SV     ^V     fV     mV     �V  
   �V     �V     �V     �V     �V     �V     �V     �V  	   W  #   W     9W  
   @W     KW  '   XW  +   �W  9   �W     �W  /   �W  c   X     �X     �X     �X     �X     �X     �X     �X     �X     Y     Y     &Y     8Y  
   IY  $   TY     yY  "   �Y     �Y     �Y     �Y  	   �Y     �Y     �Y     �Y     �Y      Z     Z     9Z     RZ  +   bZ     �Z     �Z      �Z  4   �Z  5    [     V[     \[     k[     q[     x[     �[     �[     �[     �[     �[     �[  	   \     \     \     \  #   %\     I\  	   P\  
   Z\  H   e\  \   �\  v   ]  B   �]  d   �]      *^  x   K^  D   �^  K   	_  �   U_  �   `     �`  B   �`  I   a  &   [a  0   �a  7   �a  �   �a  |   mb  ,   �b  -   c  6   Ec     |c  =   �c  8   �c  4   d  !   Fd  �   hd  E   Ke  N   �e     �e     �e     f  	   f     f  6    f     Wf     gf     mf     rf  *   xf     �f  (   �f     �f  -   g  G   2g     zg     �g     �g  >   �g     �g     �g     �g     �g     h     h  N   'h  
   vh     �h  $   �h  &   �h     �h     �h     	i  $   i  
   Ci     Ni     Ti     qi     �i  	   �i     �i     �i     �i     �i    �i     �j     �j     �j     �j  
   �j     k     "k     'k  �   +k  �    l  �   �l  �   m  �   �m  D   @n  o   �n  M   �n  X   Co  �   �o  =   8p  +   vp  3   �p  )   �p  '    q  n   (q  v   �q  $   r     3r     Mr  :   Tr     �r     �r     �r     �r     �r  0   �r  %   �r      s  %   9s     _s  �  es  .    u  6   /u  d   fu  u   �u  �   Av  {   �v  e   Pw  c   �w  q   x     �x  W   y  N   dy  �   �y  �   �z  r   F{  7   �{  i   �{  �   [|  b   �|     S}     h}  D   ~}     �}  8   �}     ~     ~  S   <~  O   �~  0   �~          (     B     K  .   g     �  %   �     �  8   �     �  D   /�  F   t�  
   ��  )   ƀ     ��  '   �     )�  /   >�     n�  $   {�     ��  B   ��  Q   �     S�  #   f�     ��  
   ��  
   ��  
   ��  
   ��  
   ��  
   ʂ  
   Ղ  
   ��  
   �  ;   ��  �   2�  |   ��     3�     D�     U�     s�  !   ��  0   ��  6   ��  '   �     ?�     S�  (   o�  <   ��     Յ     �  x   �  *   l�  &   ��  5   ��  9   �  (   .�  E   W�  Z   ��     ��  E   �     Z�     g�     z�     ��     ��     ��     ƈ  '   �  5   	�  N   ?�     ��     ��     ��     ŉ     ։     �  s   
�  �   ~�     �     "�     3�  
   H�  .   S�     ��  4   ��     ȋ  #   ֋     ��     ��      �     �     �     !�     2�     F�     Y�     i�      ��  !   ��     ̌     ��  =   �  =   %�  
   c�  
   n�  N   y�  R   ȍ  *   �  Q   F�     ��     ��  
   ��     ��  #   Ύ  #   �     �  %   )�  k   O�     ��     ۏ  '   ��     �  $   -�  A   R�     ��     ��  J   ��  (   	�  #   2�     V�  
   ^�     i�     �  ;   ��     ё     �     ��     �  6   '�     ^�  4   }�     ��  +   ˒  Q   ��  =   I�  $   ��  +   ��     ؓ  +   �  @   �     `�  7   ~�  J   ��     �     �     +�     >�     M�     [�     h�  5   ��  M   ��  8   
�  K   C�     ��     ��     ��  5   ��     �  %   ��     �     >�     Z�     f�     s�     ��     ��  +   ��     ˗  }   ؗ  �   V�     9�  ,   H�     u�  #   {�     ��     ��     ˙  #   ԙ     ��     �  O   �  \   l�  N   ɚ  *   �  !   C�  .   e�     ��     ��     ��     ��     ܛ     �     �     �     �      �  >   /�  H   n�     ��  <   ǜ  W   �     \�     b�  W   q�  �  ɝ  �   W�  �   L�    B�     W�     [�     y�  �   �     <�  �  O�  d   �     �     ��  &   ��     Ϧ     ��     �  >   �  7   ]�  ,   ��  %   §     �  ;   ��  8   9�     r�     ��     ��     ��  3   Ψ  >   �  <  A�     ~�     ��     ��     ��  !   ��  	   ë     ͫ  $   ֫  
   ��  \   �     c�     x�     ��     ��     ɬ  .   լ  
   �  1   �  [   A�      ��     ��     ӭ      �     �  $   %�     J�     j�     |�     ��     ��     Ů     ڮ  D   ��     :�  %   Q�     w�     {�     ��     ��  0   ��     �     ��     �  "   �     6�     E�  �   L�     �     	�  @   �  !   `�  w   ��     ��  �   �  �   ��     C�     a�     r�     ��     ��     ��  ,   ��     �     �  ;    �  #   <�     `�  =   m�     ��  
   ��  ,   ��  �  �  =   ��     ��     ζ  4   ܶ     �     �     #�  =   ?�  Z   }�  8   ط  r   �  (   ��  >   ��  a   �  J   N�  L   ��  T   �  K   ;�  K   ��  :   Ӻ  6   �  *   E�  >   p�     ��     ��  :   ѻ  <   �     I�  D   V�  1   ��     ͼ     ޼  #   �  >   �     S�     c�     ��     ��     ��     ��  
   ؽ  
   �     �  )   �     8�  <   K�  $   ��  
   ��     ��     ؾ  4   ��     -�     <�     O�  <   g�  K   ��  _   �     P�  [   Y�  �   ��  5   v�     ��  %   ��     ��  
   ��  ,   ��     �  7   9�  !   q�  *   ��  +   ��  2   ��     �  >   3�     r�  9   ��     ��     ��  '   ��     �     �     #�  !   4�  6   V�  2   ��  0   ��  4   ��  %   &�  a   L�  =   ��  G   ��  D   4�  F   y�  H   ��     	�  %   �     8�     G�  /   \�     ��     ��     ��  4   ��  1   ��  8   (�     a�     p�     }�     ��  _   ��     ��     �  !   !�  }   C�  �   ��  �   a�  �   �     ��  )   ��  �   ��  �   ��  c   &�  3  ��  �   ��     ��  ^   ��  g   6�  Q   ��  E   ��  I   6�  �   ��  �   W�  f   )�  K   ��  e   ��  p   B�  �   ��  k   Z�  \   ��  A   #�  �  e�  �   &�  �   ��  7   M�     ��     ��  	   ��     ��  Z   ��     �     -�     3�     8�  H   >�  4   ��  ^   ��  [   �  I   w�  �   ��  /   G�  !   w�     ��  t   ��     %�     =�  !   N�  ;   p�     ��     ��  �   ��     u�  .   ��  @   ��  M   �  .   O�  6   ~�  L   ��  Q   �      T�     u�  A   ��  7   ��     	�     �  
   %�     0�     7�     ;�  �  L�  
   !�     ,�     8�     V�  
   Z�  @   e�     ��     ��  S  ��    �    !�    :�    Q�  K   e�  �   ��  �   ��  �   /�  P  ��  �   <�  J   ��  _   �  ?   m�  T   ��  �   �  �   ��  0   ��  
   ��     ��  y   ��     Q�     g�     ��     ��     ��  K   ��  <   �  +   >�  (   j�     ��     }  �  ,  :     *  �   ;   4  <   �       =   >   ?   �   �  @   �   A   �  v   �  �           �  s   �   T  �  U   �             �  �         �     `          ?  �   �  �   �   �            7  2  Y  Q          �       �           h  x   [    �  �   �              1   �  �   z          c   R  �   �   �  y  �  &       �              �   b   #  �   �      �      y       �  �     <        �      x    E     7      4          �           w  \          �      �          n    �   �      �   �               o  d  �      �        �       M      �      �     <  �      H    �    �       -     �       ~   C       �          �   +  �           �   2   �       �  �   �   �  �   i       N   >      �   l  �  �   �                 �  f           �  �  J             �  B  �  �   E  5        8          �   �  ~    �  \  �  �      %           |       �      (  W  +  �      9  �      #      �  �  �      �   J  m   �  A  3  p      o   �                ]       �   ,   l   �    `       k  �  �      C  �   �      �  !  �    w     �   �   �   �   :          �       �   �          �   {                         �   
  !      �   �   �         �           n       �  �   >  Y   �   �           �  _      �  �   ]  A  '  }   �         �  �  �    �      �  m  6   �  �   O   9  G   �   L      �          z  �       [   
       �  $       S     3   a  Q       �  5   '      @          k       �             �  �           �  �  @  �         �   �   �   �  )      �  �  8   �   �             D   K   �  U  2          �  u       H   �           Z        �  �      =        �   �     �   4     s  �  �   �  �  �      G  �               �    �  O  �       '     _   �  {   �   �       �  1  �   �  �   e  	  ^  �      *   �  r    i  �     �  6  �           �   e       I  h   �  j  3  "     �         *  .   �               �           7  �           �  �      )  �   �  �       v     (   L   0  �  -  
    �          �               F  #      X          �  �      F   �  �  �   �    �   �   �     p   P  �   ^   �   T   �  �  8          �  /   f      D      &   �   �  :      �     �  b  !   �   �      "       �   ;         %  &  .      .    5           �    �   �   |  �    S   �   �       �   	           "  1  q     $  P   B  %  t              0   =              ;    �       �  �   N  �           �  a   X       V   �  g  �   ,  r       �   �       �   �  �  -  �   q  +       �      �       )   Z   �  M   �  g   �    �  ?  R   �  c      $  t                       �       V  �                               �      �   I       �  (  �  �       /  �          K      �  u  �      �   d   �      6  j           �   B   �  �    W                  0  �   �      �         	      �   /  9   �    

Error(s) reported:
%s         %s%% completed, speed = %s         %s%% of %s completed, ETA = %s, speed = %s   --auto                 assume default answers to questions   --changelog-first      display changelog before filelist in the description window   --justdb               update the database, but do not modify the filesystem   --media=medium1,..     limit to given media   --merge-all-rpmnew     propose to merge all .rpmnew/.rpmsave files found   --mode=MODE            set mode (install (default), remove, update)   --no-confirmation      don't ask first confirmation question in update mode   --no-media-update      don't update media at startup   --no-verify-rpm        don't verify package signatures   --parallel=alias,host  be in parallel mode, use "alias" group, use "host" machine to show needed deps   --rpm-root=path        use another root for rpm installation   --run-as-root          force to run as root   --search=pkg           run search for "pkg"   --test                 only verify if the installation can be achieved correctly   --urpmi-root           use another root for urpmi db & rpm installation   --version              print this tool's version number
  done.  failed! %d installation transactions failed %s

Is it ok to continue? %s (belongs to the skip list) %s KB %s from medium %s %s of additional disk space will be used. %s of disk space will be freed. %s of packages will be retrieved. (Not available) (This is the default) (none) - %s (medium: %s) /Add a specific _media mirror /Close /Manage _keys /P_roxy /Reload the _packages list /_About... /_Add a custom medium /_Compute updates on startup /_File /_Global options /_Help /_Media Manager /_Options /_Parallel /_Quit /_Report Bug /_Reset the selection /_Select dependencies without asking /_Show automatically selected packages /_Update /_Update media /_View <control>A <control>G <control>K <control>M <control>P <control>Q <control>R <control>U <control>W A fatal error occurred: %s. A graphical front end for browsing installed & available packages A graphical front end for installing, removing and updating packages Accessibility Add Add a host Add a key Add a medium Add a medium limit Add a parallel group Add urpmi media Add... Addable Adding a medium: Additional packages needed Adventure All All XML info files are downloaded when adding or updating media. All packages, alphabetical All packages, by group All packages, by medium repository All packages, by selection state All packages, by size All packages, by update availability All requested packages were installed successfully. All updates Already existing update media Always Apply Arcade Arch Arch. Architecture:  Archiving Are you sure you want to remove source "%s"? Are you sure you want to remove the following sources? Are you sure you want to remove the key %s from medium %s?
(name of the key: %s) Astronomy Australia Austria Backports Backup Base Because of their dependencies, the following package(s) also need to be removed: Because of their dependencies, the following package(s) must be unselected now:

 Belgium Biology Boards Books Boot and Init Brazil Browse Available Software Browse... Bugfixes updates C C++ CD-ROM Canada Cancel Canceled Cannot select Cards Cd burning Change medium Changelog: Changelog:
 Changes: Chat Checking dependencies of package... Checking validity of requested packages... Chemistry China Choose a key for adding to the medium %s Choose a medium for adding in the media limit: Choose media type Clear download cache after successful install Cluster Command Communications Compression Computer books Computer science Configuration Configure media Configure parallel urpmi (distributed execution of urpmi) Configure proxies Confirmation Conflicting Packages Console Console Tools Copying file for medium `%s'... Copyright (C) %s by ROSA Costa Rica Create media for a whole distribution Currently installed version:  Czech Republic DNS/NIS Danmark Database Databases Description not available for this package
 Description:  Details Details: Development Do not ask me next time Do nothing Do you really want to quit? Documentation Download of `%s'
speed:%s Download of `%s'
time to go:%s, speed:%s Download program to use: Downloader: Downloading package `%s'... Edit Edit a medium Edit a parallel group Edit... Editing medium "%s": Editing parallel group "%s": Editors Education Emulators Enabled Enlightenment Error Error during download Error retrieving packages Error: %s appears to be mounted read-only. Examining file of medium `%s'... Examining remote file of medium `%s'... FTP FTP server FVWM based Failure when adding medium Faqs Fatal error File tools File transfer Files: Files:
 Filter Find: Finland Firewall/Router Fonts For remote media, XML meta-data are never downloaded. For remote media, specify when XML meta-data (file lists, changelogs & informations) are downloaded. France Full set of sources GNOME GNOME Workstation GNOME and GTK+ Game station Games General updates Geosciences Germany Getting '%s' from XML meta-data... Getting information from XML meta-data from %s... Global options for package installation Global proxy settings Graphical Environment Graphical desktop Graphics Greece Group Group name: Group:  HTTP HTTP server HTTPS Hardware Help Help launched in background Here is the list of software package updates Hosts: How to choose manually your mirror However this package is not in the package list. Howtos Hungary I can't find any suitable mirror. I can't find any suitable mirror.

There can be many reasons for this problem; the most frequent is
the case when the architecture of your processor is not supported
by ROSA Linux Official Updates. I need to access internet to get the mirror list.
Please check that your network is currently running.

Is it ok to continue? I need to contact the ROSA website to get the mirror list.
Please check that your network is currently running.

Is it ok to continue? I need to contact the mirror to get latest update packages.
Please check that your network is currently running.

Is it ok to continue? IRC IceWm Desktop Icewm If you need a proxy, enter the hostname and an optional port (syntax: <proxyhost[:port]>): Importance:  In order to keep your system secure and stable, you must at a minimum set up
sources for official security and stability updates. You can also choose to set
up a fuller set of sources which includes the complete official ROSA
repositories, giving you access to more software than can fit on the ROSA
discs. Please choose whether to configure update sources only, or the full set
of sources. In order to save the changes, you need to insert the medium in the drive. Info... Information Information on packages Initializing... Inspect... Inspecting %s Inspecting configuration files... Install & Remove Software Installation failed Installation finished Installed Installing package `%s' (%s/%s)... Instant messaging Internationalization Internet station Is it ok to continue? Israel It is <b>not supported</b> by ROSA. It may <b>break</b> your system. It's impossible to retrieve the list of new packages from the media
`%s'. Either this update media is misconfigured, and in this case
you should use the Software Media Manager to remove it and re-add it in order
to reconfigure it, either it is currently unreachable and you should retry
later. Italy Japan Java KDE KDE Workstation KDE and Qt Kernel Kernel and hardware Korea Leaves only, sorted by install date Libraries Literature Local Local files Login: Looking for "README" files... Mail Mail/Groupware/News Manage keys for digital signatures of packages Matching packages: Mathematics Media limit Media limit: Medium Medium name: Medium path: Medium:  Meta packages Mirror choice Mirror list Monitoring More info More information on package... Multimedia Multimedia station NFS Name Name:  Netherlands Network Computer (client) Network Computer server Networking Never New dependencies: News No No active medium found. You must enable some media to be able to update them. No description No mirror No non installed dependency. No search results. No search results. You may want to switch to the '%s' view and to the '%s' filter No update No xml info for medium "%s", only partial result for package %s No xml info for medium "%s", unable to return any result for package %s None (installed) Norway Not installed Not selected Notice:  Office Office Workstation Ok On-demand One of the following packages is needed: Orphan packages Other Other Graphical Desktops PHP Package Package installation... Packages database is locked. Please close other applications
working with the Package database (do you have another media
manager on another desktop, or are you currently installing
packages as well?). Packages with GUI Packaging Password: Path or mount point: Perl Physics Please choose Please choose the desired mirror. Please enter your credentials for accessing proxy
 Please insert the medium named "%s" Please type in the string you want to search then press the <enter> key Please wait Please wait, adding media... Please wait, downloading mirror addresses from the ROSA website. Please wait, downloading mirror addresses. Please wait, finding available packages... Please wait, finding installed packages... Please wait, listing base packages... Please wait, listing packages... Please wait, removing medium... Please wait, removing packages... Please wait, searching... Please wait, updating media... Poland Portugal Preparing package installation transaction... Preparing package installation... Printing Problem during installation Problem during removal Protocol Protocol: Proxy hostname: Proxy settings for media "%s" Proxy... Public Keys Publishing Puzzles Python Quick Introduction Quit ROSA Linux ROSA Linux Update RPM transaction %d/%d RSYNC server Reading updates description Reason for update:  Release Remote access Removable Removable device (CD-ROM, DVD, ...) Remove Remove .%s Remove a key Remove one package? Remove %d packages? Removing package %s would break your system Removing these packages would break your system, sorry:

 Rpmdrake Rpmdrake is ROSA Linux package management tool. Rpmdrake or one of its priority dependencies needs to be updated first. Rpmdrake will then restart. Running in user mode Russia Save changes Sawfish Sciences Scientific Workstation Search aborted Search in _full package names Search results Search results (none) Security advisory Security updates Select all Select the media you wish to update: Selected Selected: %s / Free disk space: %s Server Servers Shells Singapore Size:  Slovakia Software Management Software Media Manager Software Packages Installation Software Packages Removal Software Packages Update Software Update Some additional packages need to be removed Some packages are selected. Some packages cannot be removed Some packages need to be removed Sorry, the following package cannot be selected:

%s Sorry, the following packages cannot be selected:

%s Sound Source Removal Spain Sports Starting download of `%s'... Status Stop Strategy Successfully added media %s. Successfully added media. Successfully added medium `%s'. Summary:  Sweden Switzerland System Tag this medium as an update medium Taiwan Terminals Text tools The "%s" package is in urpmi skip list.
Do you want to select it anyway? The following orphan package will be removed. The following orphan packages will be removed. The following package cannot be installed because it depends on packages that are older than the installed ones:

%s%s The following package has to be removed for others to be upgraded: The following package is going to be installed: The following %d packages are going to be installed: The following package is needed: The following packages cannot be installed because they depend on packages that are older than the installed ones:

%s%s The following packages have to be removed for others to be upgraded: The help window has been started, it should appear shortly on your desktop. The installation is finished; everything was installed correctly.

Some configuration files were created as `.rpmnew' or `.rpmsave',
you may now inspect some in order to take actions: The list of updates is empty. This means that either there is
no available update for the packages installed on your computer,
or you already installed all of them. The package "%s" was found. The specific XML info file is downloaded when clicking on package. There is already a medium by that name, do you
really want to replace it? There was a problem adding medium:

%s There was a problem during the installation:

%s There was a problem during the removal of packages:

%s There was an error downloading the mirror list:

%s
The network, or the ROSA website, may be unavailable.
Please try again later. There was an error downloading the mirror list:

%s
The network, or the website, may be unavailable.
Please try again later. These packages come with upgrade information This is an official package supported by ROSA This is an official update which is supported by ROSA. This is an unofficial update. This medium needs to be updated to be usable. Update it now ? This package contains a new version that was backported. This package is a potential candidate for an update. This package is not free software This will attempt to install all official sources corresponding to your
distribution (%s).

I need to contact the ROSA website to get the mirror list.
Please check that your network is currently running.

Is it ok to continue? To install, update or remove a package, just click on its "checkbox". To satisfy dependencies, the following package(s) also need to be installed:

 Too many packages are selected Total: %s/%s Toys True type Type Type in the hostname or IP address of the host to add: Type of medium: Type1 URL: URL:  Unable to add medium, errors reported:

%s Unable to create medium. Unable to get source packages, sorry. %s Unable to get source packages. Unable to update medium, errors reported:

%s Unable to update medium; it will be automatically disabled.

Errors:
%s United Kingdom United States Unknown Unrecoverable error: no package found for installation, sorry. Unselect all Update Update media Update sources only Update-only Updates Updating media implies updating XML info files already required at least once. Upgradable Upgrade information Upgrade information about package %s Upgrade information about this package Urpmi medium info Usage: %s [OPTION]... Use .%s as main file Use _regular expressions in searches User name: User: Verify RPMs to be installed: Verifying package signatures... Version Version:  Video View WWW Warning Warning: it seems that you are attempting to add so many
packages that your filesystem may run out of free diskspace,
during or after package installation ; this is particularly
dangerous and should be considered with care.

Do you really want to install all the selected packages? Web/FTP WindowMaker Workstation X11 X11 bitmap XML meta-data download policy: Xfce Yes You already have at least one update medium configured, but
all of them are currently disabled. You should run the Software
Media Manager to enable at least one (check it in the "%s"
column).

Then, restart "%s". You are about to add a new packages medium, `%s'.
That means you will be able to add new software packages
to your system from that new medium. You are about to add new packages media, %s.
That means you will be able to add new software packages
to your system from these new media. You are about to add new packages media.
That means you will be able to add new software packages
to your system from these new media. You are launching this program as a normal user.
You will not be able to perform modifications on the system,
but you may still browse the existing database. You can browse the packages through the categories tree on the left. You can either remove the .%s file, use it as main file or do nothing. If unsure, keep the current file ("%s"). You can view information about a package by clicking on it on the right list. You have no configured update media. ROSAUpdate cannot operate without any update media. You may also choose your desired mirror manually: to do so,
launch the Software Media Manager, and then add a `Security
updates' medium.

Then, restart %s. You may specify a user/password for the proxy authentication: You may want to update your urpmi database. You need to fill up at least the two first entries. You need to insert the medium to continue You need to select some packages first. Your medium `%s', used for updates, does not match the version of %s you're running (%s).
It will be disabled. Your medium `%s', used for updates, does not match the version of ROSA Linux you're running (%s).
It will be disabled. _: Translator(s) name(s) & email(s)
 _:cryptographic keys
Keys always bad <url> (for local directory, the path must be absolute) in descriptions in file names in names in summaries never no name found, key doesn't exist in rpm keyring! no xml-info available for medium "%s" retrieval of [%s] failed rpmdrake is already running (pid: %s) rsync Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-04-05 23:07+0400
PO-Revision-Date: 2012-04-05 23:10+0300
Last-Translator: Alexander Kazancev <kazancas@mandriva.ru>
Language-Team: Russian <gnu@mx.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: KBabel 1.11.4
X-Language: ru_RU
 

Об ошибке(ах) сообщено:
%s         %s%% завершено, скорость = %s         %s%% из %s завершено, осталось времени = %s, скорость = %s   --auto                 использовать ответ по умолчанию для всех вопросов   --changelog-first      показать журнал изменений перед списком файлов в окне с описанием   --justdb               обновить базу данных, но не изменять файловую систему   --media=источник1,..   ограничиться указанными источниками   --merge-all-rpmnew     объединить все найденные файлы .rpmnew/.rpmsave   --mode=РЕЖИМ           установить режим (install (по умолчанию), remove, update)   --no-confirmation      не выводить первый запрос на подтверждение в режиме update   --no-media-update      не обновлять источник при запуске   --no-verify-rpm        не проверять подписи пакетов   --parallel=алиас,хост  параллельный режим; используйте группу «alias», используйте машину «host», чтобы увидеть необходимые зависимости   --rpm-root=путь        использовать другой корневой каталог
                         для установки rpm-пакета   --run-as-root          принудительный запуск с правами пользователя root   --search=пакет         найти «пакет»   --test                 только проверить, можно ли установить пакет   --urpmi-root           использовать другой корень для базы данных urpmi и установки пакетов.   --version              показать номер версии данной программы
  выполнено.  не удалось! %d установочных транзакций не удались %s

Продолжить? %s (входит в список пропущенных) %s КБ %s из источника %s Будет использовано %s дискового пространства. Будет особождено %s дискового пространства. Будет загружено %s пакетов. (недоступно) (по умолчанию) (нет) - %s (источник: %s) /Добавить особое _зеркало /Закрыть /Управление _ключами /П_рокси /_Перезагрузить список пакетов /О _программе... /_Добавить пользовательский источник /_Запуск обновления при старте системы /_Файл /_Глобальные параметры /_Справка /_Менеджер источников /_Параметры /_Параллельное выполнение /В_ыход /С_ообщить об ошибке /_Очистить выбор /Автоматически _выбрать зависимости /Показывать _автоматически выбранные пакеты /_Обновить /_Обновить источник /_Вид <control>A <control>G <control>K <control>M <control>P <control>Q <control>R <control>U <control>W Произошла критическая ошибка: %s. Графический интерфейс для просмотра установленных и доступных пакетов Графический интерфейс для установки, удаления и обновления пакетов Удобство Добавить Добавить сервер Добавить ключ Добавить источник Добавить предел источника Добавить параллельную группу Добавить источник urpmi Добавить... Можно добавить Добавляется источник: Требуются дополнительные пакеты Приключения Все При добавлении или обновлении источника загружаются все XML-файлы. Все пакеты, по алфавиту Все пакеты, по группе Все пакеты, по типу источника Все пакеты, по состоянию выбора Все пакеты, по размеру Все пакеты, по доступности обновлений Все запрошенные пакеты были успешно установлены. Все обновления Уже существующий источник обновления Всегда Применить Аркады Платформа Платформа Архитектура:  Архивирование Удалить источник «%s»? Удалить следующие источники? Удалить ключ %s из источника %s?
(имя ключа: %s) Астрономия Австралия Австрия Бэкпорты Резервирование Стандартные Из-за зависимостей следующие пакеты также должны быть удалены: Из-за зависимостей выбор следующих пакетов в данный момент
должен быть отменён:

 Бельгия Биология Настольные Книги Загрузка и инициализация Бразилия Просмотр доступных программ Найти... Исправления ошибок C C++ CD-ROM Канада Отмена Отменено Не выбрано Карточные Запись CD Смените носитель Журнал изменений: Журнал изменений:
 Изменения: Чат Проверяются зависимости пакета... Проверяются запрошенные пакеты... Химия Китай Выберите ключ для добавления к источнику %s Выберите источник для установки его предела: Выберите тип источника Очисить кэш пакетов после успешной загрузки Кластер Команда Связь Сжатие Компьютерные книги Компьютерная наука Настройка Настройка источника Настроить параллельный urpmi (распределённое выполнение urpmi) Настроить прокси Подтверждение Конфликтующие пакеты Консоль Утилиты для консоли Копируется файл для источника «%s»... Copyright (C) %s ROSA Коста-Рика Создать источник для всего дистрибутива Установленная версия: Чешская Республика DNS/NIS Дания База данных Базы данных Для данного пакета нет описания
 Описание: Подробности Подробности: Разработка Не спрашивать в следующий раз Ничего не делать Вы уверены, что хотите выйти? Документация Загрузка «%s»
скорость:%s Загрузка «%s»
осталось времени: %s, скорость: %s Используемая программа загрузки: Программа загрузки: Загружается пакет «%s»... Редактировать Редактировать источник Редактировать параллельную группу Редактировать... Редактирование источника «%s»: Редактирование параллельной группы «%s»: Редакторы Образование Эмуляторы Включён Enlightenment Ошибка Ошибка загрузки Ошибка при получении пакетов Ошибка: %s примонтирован только для чтения. Изучается файл источника «%s»... Изучается удалённый файл источника «%s»... FTP Сервер FTP На основе FVWM Не удалось добавить источник FAQ'и Неисправимая ошибка Файловые утилиты Передача файла Файлы: Файлы:
 Фильтр Найти: Финляндия Файервол/маршрутизатор Шрифты Никогда не загружать XML-файл с метаданными для удалённого источника. Укажите для удалённого источника, когда нужно загружать XML-файл с метаданными (списки файлов, журналы изменений и сведения). Франция Полный набор источников GNOME Рабочая станция GNOME GNOME и GTK+ Игровая станция Игры Обычные обновления География Германия Извлекается «%s» из XML--файла с метаданными... Извлекаются данные из XML-файла с метаданными из %s... Глобальные параметры для установки пакета Общие настройки прокси Графическая среда Графический рабочий стол Графика Греция Группа Название группы: Группа: HTTP Сервер HTTP HTTPS Оборудование Справка Справка запущена в фоновом режиме Список пакетов с обновлениями программ Серверы: Как вручную выбрать своё зеркало Однако этот пакет отсутствует в списке пакетов. HOWTO Венгрия Не удаётся найти ни одного подходящего зеркала. Не удаётся найти ни одного подходящего зеркала.

Причин, вызвавших эту проблему, может быть несколько; наиболее
частым является случай, когда архитектура процессора не
поддерживается в официальных обновлениях ROSA Linux. Необходимо подключиться к Интернету, чтобы загрузить
список зеркал. Проверьте, чтобы сетевое подключение
было активным.

Продолжить? Необходимо подключиться к серверу ROSA, чтобы загрузить
список зеркал. Проверьте, чтобы сетевое подключение
было активным.

Продолжить? Необходимо подключиться к зеркалу, чтобы получить самые
последние пакеты обновлений. Проверьте, чтобы сетевое
подключение было активным.

Продолжить? IRC Рабочий стол IceWm IceWM Если требуется прокси, введите имя сервера и, при необходимости, порт (синтаксис: <прокси-сервер[:порт]>): Важность:  Чтобы поддерживать систему в надёжном и защищённом состоянии, нужно как
минимум настроить источники с официальными обновлениями безопасности. Можно
также добавить полный набор источников, в который входят официальные репозитории
пакетов ROSA, содержащие гораздо больше программного обеспечения, чем может
поместиться на дисках с дистрибутивом. Выберите, какие источники нужно настроить. Чтобы сохранить изменения, вставьте носитель в привод. Сведения... Информация Информация о пакетах Инициализация... Посмотреть... Идёт анализ %s Изучаются конфигурационные файлы Установка и удаление программ Установка была прервана Установка завершена Установлен Устанавливается пакет «%s» (%s/%s)... Обмен мгновенными сообщениями Локализация Интернет-станция Продолжить? Израиль Он <b>не поддерживается</b> ROSA. Он может <b>сломать</b> вашу систему. Невозможно получить список новых пакетов из источника «%s».
Либо этот источник обновлений неверно настроен, и в этом случае
его необходимо удалить и заново доабвить с помощью менеджера
источников программ, чтобы перенастроить его, либо источник
сейчас недоступен и следует попробовать загрузить список позднее. Италия Япония Java KDE Рабочая станция KDE KDE и Qt Ядро Ядро и оборудование Корея Только остатки, отсортированные по дате установки Библиотеки Литература Локальные файлы Локальные файлы Логин: Идёт поиск файлов «README»... Почта Почта/групповое ПО/новости Управление ключами для цифровых подписей пакетов Найденные пакеты: Математика Предел источника Предел источника: Источник Название источника: Путь к источнику: Источник: Метапакеты Выбор зеркала Список зеркал Мониторинг Дополнительно Дополнительная информация о пакете... Мультимедиа Мультимедиа-станция NFS Название Название:  Нидерланды Сетевой компьютер (клиент) Сервер сети Сеть Никогда Новые зависимости: Новости Нет Не найден активный источник. Источники необходимо включить, чтобы их можно было обновить. Описания нет Нет зеркала Нет не установленных зависимостей. Ничего не найдено. Ничего не найдено. Попробуйте переключиться на вид '%s' и фильтр '%s' Обновлений нет для источника «%s» отсутствует xml-info; для пакета %s получен только частичный результат для источника «%s» отсутствует xml-info; для пакета %s невозможно получить результат Нет (установлен) Норвегия Не установлен Не выбрано Заметка:  Офис Офисная рабочая станция ОК По запросу Нужен один из следующих пакетов: Осиротевшие пакеты Другое Другие графические рабочие столы PHP Пакет Устанавливается пакет... База данных пакетов заблокирована. Закройте другие приложения,
работающие с базой данных пакетов (может на другом рабочем
столе запущена ещё одна копия менеджера источников, или сейчас
идёт установка пакетов в другом окне?). Пакеты с графическим интерфейсом Пакеты Пароль: Путь или точка монтирования: Perl Физика Сделайте выбор Выберите предпочитаемое зеркало. Введите свои учётные данные для доступа к прокси
 Вставьте носитель с меткой «%s» Наберите в строке то что вы хотите найти и нажмите клавишу <enter> Подождите, пожалуйста Подождите. Добавляется источник... Подождите, идёт загрузка адресов зеркал с сервера ROSA. Подождите, идёт загрузка адресов зеркал. Подождите, идёт поиск доступных пакетов... Подождите, идёт поиск установленных пакетов... Подождите, составляется список пакетов... Подождите, составляется список пакетов... Подождите, удаляется источник... Подождите, удаляются пакеты... Подождите, идёт поиск... Подождите, обновляется носитель... Польша Португалия Подготовка к установке пакета... Подготовка к установке пакетов... Печать Во время установки возникла проблема Проблема во время удаления Протокол Протокол: Имя прокси-сервера: Настройки прокси для носителя «%s» Прокси... Публичные ключи Издательство Головоломки Python Краткое введение Выход ROSA Linux Обновление ROSA Linux Транзакция RPM-файла %d/%d Сервер RSYNC Считываются описания обновлений Причина обновления: Релиз Удалённый доступ Съёмный носитель Съёмное устройство (CD-ROM, DVD...) Удалить Удалить .%s Удалить ключ Удалить пакет? Удалить %d пакетов? Удаление пакета %s нарушит работу системы Удаление этих пакетов нарушит работу всей системы:

 Rpmdrake Rpmdrake — это утилита ROSA Linux для управления пакетами. Сначала нужно обновить rpmdrake или одну из зависящих от него программ. После этого rpmdrake будет снова запущен. Запуск в режиме пользователя Россия Сохранить изменения Sawfish Наука Научная рабочая станция Поиск прерван Поиск в _полных именах пакетов Результаты поиска Результаты поиска (нет) Бюллетень безопасности Обновления по безопасности Выбрать все Выберите источник для обновления: Выбранные Выбрано: %s / Свободно на диске: %s Сервер Серверы Командные процессоры Сингапур Размер:  Словакия Менеджер программ Менеджер источников программ Установка пакетов программ Удаление пакетов программ Обновление пакетов программ Обновление программ Некоторые дополнительные пакеты должны быть удалены У вас остались выбранными пакеты. Некоторые пакеты не могут быть удалены Некоторые пакеты должны быть удалены Следующий пакет невозможно выбрать:

%s Следующие пакеты невозможно выбрать:

%s Звук Удаление источников Испания Спортивные Начинается загрузка «%s»... Статус Остановить Стратегии Источник %s успешно добавлен. Источник успешно добавлен. Источник «%s» успешно добавлен. Сводка:  Швеция Швейцария Система Пометить этот источник, как источник с обновлениями Тайвань Терминалы Текстовые утилиты Пакет «%s» присутствует в списке игнорируемых.
Всё равно выбрать его? Будет удален следующий осиротевший пакет: Будут удалены следующие осиротевшие пакеты: Следующий пакет не может быт установлен, так как он зависит от пакетов, старее уже установленных:

%s%s Следующий пакет необходимо удалить. Тогда можно будет обновить другие пакеты: Для удовлетворения зависимостей будет установлен следующий пакет: Для удовлетворения зависимостей будут установлены следующие %d пакетов: Нужен следующий пакет: Следующие пакеты не могут быть установлены, так как они зависят от пакетов, старее уже установленных:

%s%s Следующие пакеты необходимо удалить. Тогда можно будет обновить другие пакеты: Справка запущена, скоро она появится на рабочем столе. Установка завершена; всё было установлено корректно.

Некоторые конфигурационные файлы созданы как `.rpmnew' или `.rpmsave',
теперь вы можете просмотреть их чтобы принять меры: Список пакетов пуст. Это означает, что либо нет обновлений для
пакетов, установленных на вашем компьютере, либо они уже все
установлены. Найден пакет «%s». После щелчка по файлу загружается XML-файл с данными. Источник с таким названием уже существует,
Заменить его? При добавлении источника возникла ошибка:

%s Во время установки возникла ошибка:

%s При удалении пакетов возникла ошибка:

%s При загрузке списка зеркал возникла ошибка:

%s
Возможно, недоступны сеть или сервер ROSA.
Попробуйте ещё раз чуть позже. При загрузке списка зеркал возникла ошибка:

%s
Возможно, недоступны сеть или сервер.
Попробуйте ещё раз чуть позже. Данные пакеты поставляются с информацией об обновлении Это официальные пакет, поддерживаемый ROSA Это официальное обновление, которое поддерживается ROSA. Это неофициальное обновление, которое <b>не поддерживается</b>. Этот источник необходимо обновить, чтобы его можно было использовать. Обновить его сейчас? Этот пакет содержит ПО из более новой версии дистрибутива. Этот пакет потенциальный кандидат для обновления. Этот пакет не является свободным ПО Будет выполнена попытка добавить все официальные источники,
соответствующие данному дистрибутиву (%s).

Необходимо подключится к серверу ROSA для получения списка зеркал.
Проверьте, чтобы сейчас было доступно подключение к Интернету.

Продолжить? Чтобы установить, обновить или удалить пакет, просто щёлкните по флажку рядом с ним. Для удовлетворения зависимостей должны быть установлены
следующие пакеты:

 Выбрано слишком много пакетов Всего: %s/%s Игрушки True Type Тип Введите имя добавляемого сервера или его IP-адрес: Тип источника: Type1 URL: URL:  Не удаётся добавить источник. Ошибки:

%s Не удаётся создать источник. Не удаётся загрузить пакеты с исходными текстами. %s Не удаётся загрузить пакеты с исходными текстами. Невозможно обновить источник. Ошибки:

%s Не удаётся обновить источник, он будет автоматически отключён.

Ошибки:
%s Объединённое Королевство Соединённые Штаты Неизвестный Неисправимая ошибка: к сожалению не найден пакет для установки. Отменить все Обновить Обновить источник Только источники с обновлениями Только обновить Обновления Под обновлением источника подразумевается как минимум одно обновление XML-файлов. Можно обновить Информация об обновлении Информация об обновлении пакета [%s] Информация об обновлении данного пакета... Сведения об источнике urpmi Использование: %s [ПАРАМЕТРЫ]... Использовать .%s в качестве главного файла Использовать _регулярные выражения в поиске Имя пользователя: Пользователь: Проверять устанавливаемые rpm-файлы: Проверяются подписи пакетов... Версия Версия:  Видео Вид WWW Внимание Предупреждение: выполняется попытка добавить слишком
много пакетов, из-за чего в файловой системе не хватит
свободного места во время или после установки пакетов; это
чрезвычайно опасно и должно быть внимательно изучено.

Установить все выбранные пакеты? Веб/FTP WindowMaker Рабочая станция X11 X11 bitmap Политика загрузки XML с метаданными: Xfce Да Источники обновлений настроены, но сейчас они все отключены.
С помощью менеджера источников программ включите хотя бы один
источник и проверьте столбец «%s»).

А затем перезапустите «%s». Будет добавлен новый источник пакетов «%s».
Это значит, что его можно будет использовать для
добавления в систему новых пакетов с программным обеспечением. Будет добавлен новый источник пакетов %s.
Это значит, что его можно будет использовать для
добавления в систему новых пакетов с программным обеспечением. Вы выбрали добавление новых источников пакетов.
Это означает, что сможете добавлять новые пакеты с программами
в вашу систему из этих новых источников. Программа запускается с правами обычного пользователя.
В систему нельзя будет внести изменения, но
можно будет просмотреть существующую базу данных. Слева показано дерево категорий пакетов. Файл .%s можно удалить, использовать его в качестве главного или ничего не делать. Если не уверены, оставьте текущий файл («%s»). Если щёлкнуть по названию пакета в списке, справа будут показаны сведения о пакете. Источники обновлений не были настроены. Программа ROSA Update не может работать без источников обновлений. Предпочитаемое зеркало можно также выбрать вручную:
для этого запустите менеджер источников программ, а затем
добавьте источник «Обновления по безопасности».

Затем перезапустите %s. Можно указать пользователя/пароль для аутентификации на прокси-сервере: Возможно, нужно обновить базу данных urpmi. Необходимо заполнить как минимум два первых пункта. Вставьте носитель для продолжения Необходимо сначала выбрать какие-либо пакеты. Источник «%s», используемый для обновлений, не соответствует текущей версии %s (%s).
Источник будет отключён. Источник «%s», используемый для обновлений, не соответствует текущей версии ROSA Linux (%s).
Источник будет отключён. Павел Марьянов <acid_jack@ukr.net>
 Ключи всегда неверный <url> (для локального каталога путь должен быть абсолютным) в описаниях в именах файлов в именах в сводках никогда имя не найдено, ключ отсутствует в rpm keyring! в источнике «%s» отсутствует xml-info не удалось загрузить [%s] rpmdrake уже запущен (pid: %s) rsync 